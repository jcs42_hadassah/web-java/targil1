package com.hadassah;


/**
 * targil1
 *
 * @author yosephmi
 */
public class Main {
    /**
     * @param args - array of strings, input and output paths
     */
    public static void main(String[] args) {
        try {
            URLSortByContentLength runner = new URLSortByContentLength(args);
            runner.execute();
        } catch (IllegalArgumentException e) {
            //when the cmd line args are bad
            System.out.println("wrong usage");
        } catch (InputSupplierException e) {
            //when there is issue with input file(doesnt exist, error whilst reading)
            System.out.println("bad input");
        } catch (Exception ignore) { }
    }
}