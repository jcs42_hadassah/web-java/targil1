package com.hadassah;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * implements the abstract InputSupplier interface
 * reads from a file line by line
 */
public class FileReaderLineByLine implements InputSupplier {
    private String m_file_path;
    private BufferedReader m_inputStream;
    //-------------------------------------------------------------------------

    /**
     * @param filePath - path to input file
     * @throws InputSupplierException - when the file doesnt exist
     */

    FileReaderLineByLine(String filePath) throws InputSupplierException {
        //I think checking from constructor right away if file exists is better
        File file = new File(filePath);
        if (!file.exists())
            throw new InputSupplierException(String.format("File: %s does not exist", filePath));
        this.m_file_path = filePath;
    }
    //-------------------------------------------------------------------------

    /**
     * @return List of strings that holds input from file read line by line
     * @throws InputSupplierException from reading or opening file
     */
    @Override
    public List<String> getInputs() throws InputSupplierException {
        List<String> lines = new ArrayList<>();
        try {
            m_inputStream = new BufferedReader(new FileReader(m_file_path));
            String curr_line;

            //iterating through file line by line and appending
            while ((curr_line = m_inputStream.readLine()) != null) {
                lines.add(curr_line);
            }
        } catch (Exception e) {
            throw new InputSupplierException();
        }
        return lines;
    }

    /**
     * closing the file, when its not null
     *
     * @throws IOException when closed fail, chose not to catch here so user can decide how to handle
     */

    public void close() throws IOException {
        if(m_inputStream!=null)
            m_inputStream.close();
    }
}
