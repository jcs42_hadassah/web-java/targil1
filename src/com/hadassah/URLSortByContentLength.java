package com.hadassah;

import java.io.IOException;
import java.util.List;

/**
 * class that allows one to use this program over and over in single program.
 * it is cleaner to put in main, but then it is like singleton.
 *
 * the class is a wrapper of the exercise
 */
public class URLSortByContentLength {
    private URLStatsList m_allStats; //holds a list of URLStat objects
    private String m_InputPath;
    private String m_OutputPath;
    //-------------------------------------------------------------------------

    /**
     * @param args - the programs needed arguments, input and output file paths(input must be existing path,
     *             output will be created if it does not exist
     * @throws IllegalArgumentException - when there is not exactly 2 arguments given
     */
    public URLSortByContentLength(String[] args) throws IllegalArgumentException {
        //TODO, validate here the input output paths?
        if (args.length != 2) {
            throw new IllegalArgumentException();
        }
        m_InputPath = args[0];
        m_OutputPath = args[1];
        m_allStats = new URLStatsList();
    }

    /**
     * a driver for the program.
     * basically the controller, the reason i want this in a class and not in main is if we want to run it a few times,
     * with a few input files.
     *
     * @throws InputSupplierException - when there is an issue reading from the input.
     */
    public void execute() throws InputSupplierException {
        this.setURLSfromSource();
        this.m_allStats.sort();
        this.write();
    }

    /**
     * get the url as string, create a URL object and activate all its stats(currently its the content length only)
     * and append to URLStatsList object
     *
     * @param url - string that is the url
     */

    public void append(String url) {
        try {
            URLStats u = new URLStats(url); //throws malformed
            u.count_content_length();  //throws unknown host exception
            m_allStats.add(u);
        }
        //wanting to skip this link if there is an exception with the url or its stats
        catch (IOException e) {
            //TODO, should we remove print?
//            System.out.println(String.format("skipping url: %s", url));
        }
    }

    /**
     * goes over the inputs and for each url creates the URLStats object (currently only stat is content length)
     * @throws InputSupplierException if there is a problem reading the file content or opening file
     */
    private void setURLSfromSource() throws InputSupplierException {
        FileReaderLineByLine input = new FileReaderLineByLine(m_InputPath);

        List<String> inputs = input.getInputs();
        inputs.forEach(this::append);

        try {
            input.close();
        } catch (IOException ignore) { } //nothing we want to do on failure of closing
    }

    /**
     * writes the list of urls to the file
     */
    private void write() {
        //TODO, what if the file isn't good?, should i print "bad input" for example if the given path is that of a directory
        try (WriteToFile writer = new WriteToFile(m_OutputPath)) {
            writer.write(m_allStats.toString());
        } catch (Exception ignored) {
        }
    }
}