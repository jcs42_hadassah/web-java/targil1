package com.hadassah;

/**
 * custom exception that gets thrown for program input errors.
 */
public class InputSupplierException extends Exception {
    public InputSupplierException() {

    }

    public InputSupplierException(final String cause) {
        super(cause);
    }
}
