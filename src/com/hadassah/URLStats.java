package com.hadassah;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * class that holds a bunch of stats of web page
 * currently it only holds the url and its content length
 */
public class URLStats implements Comparable<URLStats> {
    private final URL m_url;
    private Integer m_content_length;
    //-------------------------------------------------------------------------

    /**
     *
     * @param url the string for the url
     * @throws MalformedURLException will get thrown when the given string is not valid
     */
    URLStats(String url) throws MalformedURLException {
        m_url = new URL(url);
    }

    /**
     *
     * @param other the other URLStats to compare to
     * @return 0 if equal,
     *        greater than zero if other is greater than this,
     *        less than zero when this is greater than other
     */
    @Override
    public int compareTo(URLStats other) {
        return this.m_content_length.compareTo(other.getM_content_length());
    }

    /**
     *
     * @return string representation of the URLStats object
     */
    @Override
    public String toString() {
        return
                m_url + " " + m_content_length;
    }

    /**
     *
     * @return the content length
     */
    public Integer getM_content_length() {
        return m_content_length;
    }

    /**
     * counts the amount of bytes on webpage, if there is an error during. it will be 0
     * @throws IOException - when there is an error connecting to the page
     */
    public void count_content_length() throws IOException {
        int content_length = 0;
//        the reason we want to open every time and not memoize is because web pages can be
//        very dynamic, changing every few seconds
        InputStream input = new BufferedInputStream(m_url.openStream());
        try {
            while ((input.read()) != -1) {
                content_length++;
            }
        } catch (Exception e) {
            content_length = 0;
        } finally {
            m_content_length = content_length;
        }
    }
}
