package com.hadassah;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * class that can write to file, AutoCloseable
 * currently i did not implement the writes as interface and inheritance,
 * because we then have to deal with different types
 */
public class WriteToFile implements AutoCloseable {
    private final Writer m_writer;
    //-------------------------------------------------------------------------

    /**
     * Constructor
     *
     * @param path - path to output file
     * @throws IOException if the named file exists but is a directory rather
     *                     than a regular file, does not exist but cannot be
     *                     created, or cannot be opened for any other reason
     */
    WriteToFile(String path) throws IOException {
        m_writer = new FileWriter(path);
    }

    /**
     * @param s - string to write to file
     * @throws IOException - when the write to file fails
     */
    public void write(String s) throws IOException {
        m_writer.write(s);
    }

    /**
     * overrided close function
     *
     * @throws Exception - when the close fails
     */
    @Override
    public void close() throws Exception {
        //making sure to flush before close
        m_writer.flush();
        m_writer.close();
    }
}