package com.hadassah;

import java.util.List;


/**
 * Abstract interface for program input
 */
public interface InputSupplier {
    /**
     *
     * @return the inputs
     * @throws InputSupplierException if there is an error when reading program input
     */
    List<String> getInputs() throws InputSupplierException;
}
