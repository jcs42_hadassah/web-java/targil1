package com.hadassah;


import java.util.ArrayList;
import java.util.Collections;

import java.util.List;
import java.util.stream.Collectors;

/**
 * wrapper for list of URLStats
 */
public class URLStatsList {
    private final List<URLStats> m_url_counts;
    //--------------------------------------------------

    /**
     * sets the array
     */
    URLStatsList() {
        m_url_counts = new ArrayList<>();
    }

    /**
     * iterates through the array of URLStats, and strigifies them all together
     * @return string of all the URLStats
     */
    @Override
    public String toString() {
        return m_url_counts.stream().map(Object::toString)
                .collect(Collectors.joining("\n"));
    }

    /**
     *
     * @param u a URLStats object to append to list
     */

    public void add(URLStats u) {
        m_url_counts.add(u);
    }
    /**
     * sorts the URLStats list by its content length
     */
    public void sort() {
        Collections.sort(m_url_counts);
    }
}
